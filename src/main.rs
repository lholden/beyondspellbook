extern crate glob;

extern crate serde;
extern crate serde_json;
extern crate scraper;
#[macro_use]
extern crate serde_derive;

use std::fs::File;
use std::io::prelude::*;

use glob::glob;

fn get_domain(name: &str) -> &'static str {
    match name {
        "Fog Cloud" => "Tempest",
        "Thunderwave" => "Tempest",
        "Gust of Wind" => "Tempest",
        "Shatter" => "Tempest",
        "Call Lightning" => "Tempest",
        "Sleep Storm" => "Tempest",
        "Control Water" => "Tempest",
        "Ice Storm" => "Tempest",
        "Destructive Wave" => "Tempest",
        "Inspect Plague" => "Tempest",
        _ => "",
    }
}

use scraper::{Html, Selector, ElementRef};

#[derive(Debug, Default, Serialize)]
struct Spell {
    name: String,
    desc: String,
    higher_level: String,
    page: String,
    range: String,
    components: String,
    material: String,
    ritual: String,
    duration: String,
    concentration: String,
    casting_time: String,
    level: String,
    school: String,
    class: String,
    archetype: String,
    domains: String,
    circles: String,
    oaths: String,
    patrons: String,
}

fn extras() -> Vec<Spell> {
    vec![
        Spell {
            name: "Destructive Wave".into(),
            desc: "You strike the ground, creating a burst of divine energy that ripples outward from you. Each creature you choose within 30 feet of you must succeed on a Constitution saving throw or take 5d6 thunder damage, as well as 5d6 radiant or necrotic damage (your choice), and be knocked prone. A creature that succeeds on its saving throw takes half as much damage and isn't knocked prone. ".into(),
            range: "Self (30 ft)".into(),
            components: "V".into(),
            duration: "Instantaneous".into(),
            school: "Evocation".into(),
            domains: get_domain("Destructive Wave").into(),
            casting_time: "1 Action".into(),
            class: "Paladin".into(),
            level: "5th-Level".into(),
            .. Spell::default()
        },
    ]
}

fn get(er: &ElementRef, sel: &Selector) -> String {
    er.select(sel).next().unwrap().text()
        .map(|v| v.trim())
        .map(|v| if v.is_empty() {" "} else {v})
        .collect()
}

fn get_block(er: &ElementRef, slug: &str) -> Vec<String> {
    let block_sel = Selector::parse(&format!("div.ddb-statblock-item-{}", slug)).unwrap();
    let value_sel = Selector::parse("div.ddb-statblock-item-value").unwrap();

    let block = er.select(&block_sel).next().unwrap();

    block.select(&value_sel).next().unwrap().text()
        .map(|v| v.trim().to_string())
        .filter(|v| !v.is_empty())
        .collect()
}

fn get_duration(er: &ElementRef) -> (String, String) {
    let values = get_block(er, "duration");

    if values[0] == "Concentration" {
        (values[1].clone(), "YES".to_string())
    }
    else {
        (values[0].clone(), "NO".to_string())
    }
}

fn get_components(er: &ElementRef) -> String {
    get_block(er, "components")[0].clone()
}

fn get_classes(er: &ElementRef) -> String {
    let classes_sel = Selector::parse("div.more-info-footer-classes > div.tag").unwrap();

    er.select(&classes_sel)
        .map(|v| v.inner_html())
        .collect::<Vec<String>>()
        .join(", ")
}

fn get_description(er: &ElementRef) -> (String, String) {
    let description_sel = Selector::parse("div.more-info-body-description").unwrap();
    let material_sel = Selector::parse("span.components-blurb").unwrap();

    let desc = er.select(&description_sel).next().unwrap().inner_html();
    if er.select(&material_sel).count() < 1 {
        return (desc, "".into())
    }
    
    let mat_html = er.select(&material_sel).next().unwrap().html().trim().to_string();
    let desc2: String = desc.split(&mat_html)
        .filter(|v| v.trim() != mat_html)
        .collect::<Vec<&str>>()
        .join("");

    let mat_doc = Html::parse_fragment(&mat_html);
    let mat = mat_doc.select(&material_sel).next().unwrap();

    (desc2, mat.inner_html())
}

fn get_level(er: &ElementRef) -> String {
    let level_sel = Selector::parse("div.spell-level > span").unwrap();

    let mut level = get(er, &level_sel);
    if level != "Cantrip" {
        level = format!("{}-Level", level);
    }
    level
}

fn get_school(er: &ElementRef) -> String {
    get_block(er, "school")[0].clone()
}

fn get_name(er: &ElementRef) -> (String, String) {
    let name_group_sel = Selector::parse("div.spell-name > span.name").unwrap();
    let name_sel = Selector::parse("a.link").unwrap();
    let ritual_sel = Selector::parse("i.i-ritual").unwrap();

    let name_group = er.select(&name_group_sel).next().unwrap();
    let name = get(&name_group, &name_sel);

    if name_group.select(&ritual_sel).count() > 0 {
        (name, "YES".to_string())
    }
    else {
        (name, "NO".to_string())
    }
}

fn make_spell(info: &ElementRef, more_info: &ElementRef) -> Spell {
    let cast_time_sel = Selector::parse("div.spell-cast-time > span").unwrap();
    let range_sel = Selector::parse("div.spell-range").unwrap();

    let (name, ritual) = get_name(&info);
    let (description, material) = get_description(&more_info);
    let (duration, conc) = get_duration(&more_info);

    Spell {
        name: name.clone(),
        desc: description,
        material: material,
        duration: duration,
        concentration: conc,
        ritual: ritual,
        range: get(&info, &range_sel),
        casting_time: get(&info, &cast_time_sel),
        components: get_components(&more_info),
        level: get_level(&info),
        school: get_school(&more_info),
        class: get_classes(&more_info),
        domains: get_domain(&name).to_string(),
        .. Spell::default()
    }
}

fn parse(file_buf: &str) -> Vec<Spell> {
    let doc = Html::parse_document(file_buf);

    let listing_body_sel = Selector::parse("div.listing-body > ul").unwrap();
    let info_sel = Selector::parse("div.info").unwrap();

    let listing_body = doc.select(&listing_body_sel).next().unwrap();

    let mut spells: Vec<Spell> = Vec::new();
    for div_ref in listing_body.select(&info_sel) {
        let div = div_ref.value();
        let slug = div.attr("data-slug").unwrap();

        let more_info_sel = Selector::parse(&format!("div.more-info-spell-{}", slug)).unwrap();
        let more_info = doc.select(&more_info_sel).next().unwrap();

        spells.push(make_spell(&div_ref, &more_info));
    }

    spells
}

fn main() {
    let mut spells: Vec<Spell> = Vec::new();

    for entry in glob("dump/*.html").expect("Working glob pattern") {
        let entry = entry.expect("To find files");

        println!("Running file: {}", entry.display());
        let mut file = File::open(entry).unwrap();
        let mut file_buf = String::new();
        file.read_to_string(&mut file_buf).unwrap();

        spells.append(&mut parse(&file_buf));
    }

    spells.append(&mut extras());

    let output = serde_json::to_string(&spells).unwrap();

    let mut file_out = File::create("spells.json").unwrap();
    file_out.write_all(output.as_bytes()).unwrap();

    let output2 = format!("var jsonSpellData = {}", output);
    let mut file_out2 = File::create("spells.js").unwrap();
    file_out2.write_all(output2.as_bytes()).unwrap();
}
